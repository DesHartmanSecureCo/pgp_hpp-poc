import axios from 'axios';

const SecureCoAPI = {     // This is the Object

    debug: true,    // logs verbosely to console
    bearer: process.env.VUE_APP_SECURECO_BEARER,

    axios_instance: axios.create({
        baseURL: process.env.VUE_APP_SECURECO_URL,  //This allows us to change the rest of the URL
        headers: {
            'Authorization': 'bearer ' + process.env.VUE_APP_SECURECO_BEARER, //this.bearer,
            'content-type': 'application/json'
        },
        timeout: 3000
    }),
    // Get Bearer values from the environmental variables
    
    sessionId: '',                  // Session ID
    endPoint: '',                   // Semafone Endpoint. "" means not in Secure Mode
    pollingCRN: null,               // Interval holder object
    pollingCard: null,              // Interval holder object
    panStatus: '',                  // Semafone mode: ACTIVE, COMPLETE
    cvcStatus: '',                  // Semafone mode: ACTIVE, COMPLETE
    captureStatus: '',              // Semafone mode: ACTIVE, COMPLETE
    loopCount: 0,                   // Polling loop. Limits polling.

    getNextCrn(ccp_data) {
        let theUrl = '/V2/semafone/getnextcrn';
        this.axios_instance.get(theUrl)
            .then(response => {
                /* Response structure
                    {
                        "APIResponseCode":0,
                        "APIResponseMessage":"Success",
                        "APIData":
                            {
                            "CRNumber": "5049",
                            "SessionId": "dpma-f0d39ca4-806a-4627-bbb9-d5f6e67ad2ec-61643493"
                            }
                    }
                */
                //Check a valid CRN number was received
                if (response.data.APIResponseMessage == "Success") {
                    ccp_data.CRN = response.data.APIData.CRNumber;
                    this.sessionId = response.data.APIData.SessionId;

                    if (this.debug) console.log(`getNextCrn: response.data: ${JSON.stringify(response.data)}`);
                    if (this.debug) console.log(`getNextCrn: Got a CRN: ${ccp_data.CRN}, Polling for entry point now: ${JSON.stringify(this)}`);
/*                  
                    // Calling setInterval has issues with "this"
                    // See this article https://stackoverflow.com/questions/43335477/how-to-use-setinterval-in-vue-component
                    this.pollingCRN = setInterval(this.getSecureSession().bind(this), 1000);   // This does not work
                    this.pollingCRN = setInterval(self.getSecureSession(), 1000);   // This does not work
*/
                    this.pollingCRN = setInterval(() => this.getSecureSession(ccp_data), 1000);   // Has to be Arrow to get correct "this"
                } else {
                    if (this.debug) console.log(`getNextCrn: API Error: APIResponseMessage = ${response.data.APIResponseMessage}`);
                }
            })
            .catch(function (error) {
                if (this.debug) console.log(`getNextCrn: Got Axios Error: ${error}`);
            }.bind(this));  // You need to bind "this" to keep variable usage scoped
    },

    getSecureSession(ccp_data) {
        let theUrl = '/V2/semafone/getsecuresession';
        let theData = {
            crNumber: ccp_data.CRN,
            sessionId: this.sessionId
        };
        if (this.debug) console.log(`getSecureSession: POST data: ${JSON.stringify(theData)}`);  //Log as a JSON object

        this.axios_instance.post(theUrl, theData)
            .then(response => {
                /* Response structure
                    {
                        {
                        "APIResponseCode":0,
                        "APIResponseMessage":"Success",
                        "APIData":
                            {
                            "crNumber": "5049",
                            "sessionId": "dpma-f0d39ca4-806a-4627-bbb9-d5f6e67ad2ec-61643493"
                            "endPoint": "https://syd2sema001.int.secureco.co:443"
                            }
                    }
                */
                if (this.debug) console.log(`getSecureSession: Updating endpoint - response.data: ${response.data}`);
                this.endPoint = response.data.APIData.endPoint;
                

                if (this.endPoint === "") {
                    if (this.debug) console.log(`getSecureSession: Did not get an Endpoint ###########. Count is ${this.loopCount}`);
                    if (this.loopCount < 20){
                        this.loopCount++;
                        if (this.debug) console.log(`Increased this.loopCount to ${this.loopCount}`);
                    } else {
                        this.loopCount = 0;
                        if (this.debug) console.log(`this.loopCount done ${this.loopCount}`);
                        if (this.debug) console.log(`Stopping CRN polling`);
                        clearInterval(this.pollingCRN);
                    }
                } else {
                    this.sessionId = response.data.APIData.sessionId;
                    if (this.debug) console.log(`getSecureSession: SessionID returned updated to: ${this.sessionId}`);

                    if (this.debug) console.log(`getSecureSession: Got an Endpoint Stop CRN polling`);
                    clearInterval(this.pollingCRN);

                    if (this.debug) console.log(`getSecureSession: Secured Session. Change status to SECURED`);
                    ccp_data.secureIndicator = true;
                    
                    if (this.debug) console.log(`getSecureSession: Starting Card details polling now`);
                    this.pollingCard = setInterval(() => this.getSecureCardDetails(ccp_data), 1000); // Has to be Arrow to get correct "this"
                }
            })
            .catch(function (error) {
                if (this.debug) console.log(`getSecureSession - Got Axios Error: ${JSON.stringify(error)}`);

                if (this.debug) console.log(`Stopping CRN polling`);
                clearInterval(this.pollingCRN);
            }.bind(this));  // You need to bind "this" to keep variable usage scoped

    },
    getSecureCardDetails(ccp_data) {
        let theUrl = '/V2/semafone/getsecurecarddetails';
        let theData = {
            crNumber: ccp_data.CRN,
            sessionId: this.sessionId,
            endPoint: this.endPoint
        };

        if (this.debug) console.log(`getSecureCardDetails: POST data: ${JSON.stringify(theData)}`);

        this.axios_instance.post(theUrl, theData)
            .then(response => {
                /* Response structure
                    {
                        "APIResponseCode":0,
                        "APIResponseMessage":"Success",
                        "APIData":
                            {
                                "semafoneResponse":
                                    {
                                        "crNumber":"10504",
                                        "sessionId":" dpmb-355462f2-8940-458e-9141-eac527ac4ad2-36281345",
                                        "endPoint":"https://syd2sema001.int.secureco.cc:443"},
                                        "panMask":"**** **** **** 1111",
                                        "panCount":16,
                                        "panLength":"16",
                                        "panStatus":"COMPLETE",
                                        "cvcMask":"***",
                                        "cvcCount":3,
                                        "cvcLength":"3",
                                        "cvcStatus":"COMPLETE",
                                        "panType":"VISA",
                                        "captureStatus":"COMPLETE"
                                    }
                            }
                    }
                */
                //if (this.debug) console.log(`getSecureCardDetails: response.data.APIData: ${JSON.stringify(response.data.APIData)}`);
                ccp_data.panMask = response.data.APIData.panMask;
                ccp_data.cvcMask = response.data.APIData.cvcMask;
                ccp_data.panType = response.data.APIData.panType;

                if (this.debug) console.log(`getSecureCardDetails: #### ccp_data.panMask #####: ${JSON.stringify(ccp_data.panMask)}`);

                // Check if all data has been received and end Secured Session.
                if (response.data.APIData.captureStatus === "COMPLETE") {
                    if (this.debug) console.log(`getSecureCardDetails: Got all details, exiting Secure Status..........`);
                    ccp_data.secureIndicator = false;
                    clearInterval(this.pollingCard);
                }

            })
            .catch(function (error) {
                if (this.debug) console.log(`getSecureCardDetails - Axios Error: ${JSON.stringify(error)}`);
                ccp_data.secureIndicator = false;
                clearInterval(this.pollingCard);
            }.bind(this));  // You need to bind "this" to keep variable usage scoped
    },

    submit(ccp_data) {
        // Stop the polling
        //clearTimeout(this.pollingCard);

        let theUrl = '/V2/transactions/purchase';
        let theData = {
                    SemafoneData:
                    {
                        CRNumber: ccp_data.CRN,
                        sessionId: this.sessionId,
                        endPoint: this.endPoint
                    },
                    PaymentData:
                    {
                        MerchantAccountID: process.env.VUE_APP_SECURECO_MID,
                        RequestID: 'SecureCo_V1_R2_41',
                        RequestedAmount: 3000,
                        Currency :'AUD',
                        OrderNumber: 'SEC0024',
                        PaymentMethod: 'creditcard', //process.env.VUE_APP_SECURECO_TX_TYPE,
                        AccountHolder:
                        {
                            FirstName: 'Des',
                            LastName: 'Tester',
                        },
                        Card:
                            {
                                ExpirationMonth: '01',
                                ExpirationYear: '2021',
                            }
                    }
        };

        if(this.captureStatus == 'COMPLETE') {
            // console.log('captureStatus: ' + this.captureStatus);
            if (this.debug) console.log("Submit Purchase: theData: %j", theData);  //Log as a JSON object
            this.axios_instance.post(theUrl, theData)
                .then(response => {
                    /* Response structure
                        {
                            {
                            "APIResponseCode":0,
                            "APIResponseMessage":"Success",
                            "APIData":
                                {
                                "crNumber": "5049",
                                "sessionId": "dpma-f0d39ca4-806a-4627-bbb9-d5f6e67ad2ec-61643493"
                                "endPoint": "https://syd2sema001.int.secureco.co:443"
                                }
                        }
                    */
                    // if (this.debug) console.log("response.data: %j", response.data);  //Log as a JSON object
                    this.endPoint = response.data.APIData.endPoint;
                })
                .catch(function (error) {
                    if (this.debug) console.log('getSecureSession - Axios Error: ' + error);
                }.bind(this));  // You need to bind "this" to keep variable usage scoped
        }
    },

    resetPan(ccp_data) {
        let theUrl = '/V2/semafone/resetpan';
        let theData = {
            crNumber: ccp_data.CRN,
            sessionId: this.sessionId,
            endPoint: this.endPoint
        };
        this.axios_instance.post(theUrl, theData)
            .then(response => {
                /* Response structure
                    {
                        "APIResponseCode":0,
                        "APIResponseMessage":"Success",
                        "APIData":"True"
                    }
                */
                if (this.debug) console.log(`Reset PAN response.data: ${response.data.APIData}`);
                ccp_data.panMask = '';
                ccp_data.panType = '';
            })
            .catch(function (error) {
                if (this.debug) console.log(`getSecureCardDetails - Axios Error: ${error}`);
            }.bind(this));  // You need to bind "this" to keep variable usage scoped
    },
    resetCvc(ccp_data) {
        let theUrl = '/V2/semafone/resetcvc';
        let theData = {
            crNumber: ccp_data.CRN,
            sessionId: this.sessionId,
            endPoint: this.endPoint
        };
        this.axios_instance.post(theUrl, theData)
            .then(response => {
                /* Response structure
                        "APIResponseCode":0,
                        "APIResponseMessage":"Success",
                        "APIData":"True"
                    }
                */
                if (this.debug) console.log(`Reset CVC response.data: ${response.data.APIData}`);
                ccp_data.cvcMask = '';
            })
            .catch(function (error) {
                if (this.debug) console.log(`getSecureCardDetails - Axios Error: ${error}`);
            }.bind(this));  // You need to bind "this" to keep variable usage scoped
    },
    resetSession(ccp_data) {
        let theUrl = '/V2/semafone/resetsession';
        let theData = {
            crNumber: ccp_data.CRN,
            sessionId: this.sessionId,
            endPoint: this.endPoint
        };
        this.axios_instance.post(theUrl, theData)
            .then(response => {
                /* Response structure
                    {
                        "APIResponseCode":0,
                        "APIResponseMessage":"Success",
                        "APIData":"True"
                    }
                */
                if (this.debug) console.log(`Reset Session response.data: ${response.data.APIData}`);
                ccp_data.panMask = '';
                ccp_data.cvcMask = '';
                ccp_data.panType = '';
                this.captureStatus = '';
            })
            .catch(function (error) {
                if (this.debug) console.log(`getSecureCardDetails - Axios Error: ${error}`);
            }.bind(this));  // You need to bind "this" to keep variable usage scoped

    }
};  

export default SecureCoAPI;